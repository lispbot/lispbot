(in-package #:lispbot)

(defclass scheduler ()
  ((actions :initform nil)
   (lock :initform (bt:make-recursive-lock))
   (notifier)))

(defgeneric schedule-action (scheduler action))

;; Implementation

(defun scheduler-run-actions (scheduler)
  (with-slots (actions lock) scheduler
    (let ((actions-to-run))
      (bt:with-recursive-lock-held (lock)
        (setf actions-to-run actions)
        (setf actions nil))
      (loop for act in (nreverse actions-to-run)
         do (funcall act)))))

(defmethod schedule-action ((self scheduler) action)
  (with-slots (actions lock notifier) self
    (bt:with-recursive-lock-held (lock)
      (push action actions))
    (as:trigger-notifier notifier)))

(defmethod initialize-instance :after ((self scheduler) &key)
  (setf (slot-value self 'notifier)
        (as:make-notifier (lambda () (scheduler-run-actions self))
                          :single-shot nil)))
