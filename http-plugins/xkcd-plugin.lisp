(in-package :lispbot.http-plugins)

(defclass xkcd-plugin (plugin)
  ((last-update :initform nil)
   (last-timestamp :initform nil))
  (:default-initargs :name "xkcd"))

(defmethod help ((self xkcd-plugin))
  (help-for-commands self))

(defun get-xkcds ()
  (bb:alet ((html (das:http-request "https://xkcd.com/archive/")))
     (let* ((document (chtml:parse html (cxml-stp:make-builder)))
            (res nil))
        (stp:do-recursively (a document)
          (when (and (typep a 'stp:element)
                     (equal (stp:local-name a) "a"))
            (cl-ppcre:register-groups-bind (num)
                ("^/([0-9]+)/$" (stp:attribute-value a "href"))
              (push (cons num (stp:string-value a)) res))))
        res)))

(defun find-xkcd (pattern xkcds)
  (or (find pattern xkcds :test #'string= :key #'car)
      (let ((scanner (cl-ppcre:create-scanner pattern :case-insensitive-mode t)))
        (find-if (lambda (x) (cl-ppcre:scan scanner (cdr x))) xkcds))))

(defun string-join (delim list)
  (with-output-to-string (s)
    (when list (format s "~a" (first list)))
    (dolist (l (rest list))
      (format s "~a~a" delim l))))

(defun maybe-update (self)
  (with-slots (last-update last-timestamp) self
    (let ((now (get-universal-time)))
      (when (or (not last-timestamp) (< last-timestamp (- now (* 60))))
        (bb:alet ((xkcds (get-xkcds)))
          (setf last-update xkcds
                last-timestamp now)
          xkcds))
      (bb:promisify last-update))))

(defcommand xkcd ((self xkcd-plugin) &rest args)
  "search xkcd comics by title"
  (if (endp args)
      (error "Too few arguments for xkcd")
    (bb:alet ((xkcds (maybe-update self)))
      (let* ((res (find-xkcd (string-join " " args) xkcds)))
        (if res (reply (format nil "https://xkcd.com/~a/ - ~a" (car res) (cdr res)) t)
            (reply "Sorry, nothing found" t))))))
