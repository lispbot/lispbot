(in-package :lispbot.http-plugins)

(defclass tinyurl-plugin (plugin)
  ()
  (:default-initargs :name "tinyurl"))

(defmethod help ((self tinyurl-plugin))
  (help-for-commands self))

(defparameter *tinyurl-url* "http://www.tinyurl.com/api-create.php")

(defun tinyurl-get (url)
  (das:http-request *tinyurl-url* :parameters `(("url" . ,url))))

(defcommand tinyurl ((self tinyurl-plugin) url)
  "get a tiny url from tinyurl"
  (declare (ignore self))
  (bb:alet ((res (tinyurl-get url))
         (*last-message* *last-message*))
     (reply res)))
