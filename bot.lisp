 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; __                               ____            __             ;;
;;/\ \       __                    /\  _`\         /\ \__          ;;
;;\ \ \     /\_\    ____  _____    \ \ \L\ \    ___\ \ ,_\         ;;
;; \ \ \  __\/\ \  /',__\/\ '__`\   \ \  _ <'  / __`\ \ \/         ;;
;;  \ \ \L\ \\ \ \/\__, `\ \ \L\ \   \ \ \L\ \/\ \L\ \ \ \_        ;;
;;   \ \____/ \ \_\/\____/\ \ ,__/    \ \____/\ \____/\ \__\       ;;
;;    \/___/   \/_/\/___/  \ \ \/      \/___/  \/___/  \/__/       ;;
;;                          \ \_\                                  ;;
;;                           \/_/                                  ;;
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :lispbot)

 ;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                         ;;
;; Configuration variables ;;
;;                         ;;
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar *default-data-directory*
  (merge-pathnames ".lispbot/" (user-homedir-pathname))
  "the default place where the bot and plugins will create their files")

(defvar *debug* nil
  "print debug output?")

 ;;;;;;;;;;;;;;
;;            ;;
;; Bot Class  ;;
;;            ;;
 ;;;;;;;;;;;;;;

(defclass bot ()
  ((connection
    :initform nil
    :reader connection
    :type (or null irc:connection)
    :documentation "internal connection representation")
   (channels
    :initform nil
    :reader channels
    :documentation "list of channels the bot has joined")
   (autojoin-channels
    :initform nil
    :reader autojoin-channels
    :documentation "list of channels the bot should join")
   (plugins
    :initform nil
    :accessor plugins
    :documentation "set of plugins for this bot")
   (nick
    :initform "lispbot"
    :initarg :nick
    :reader nick
    :type string
    :documentation "the nickname of the bot")
   (quit-message
    :initform "https://gitlab.com/lispbot/lispbot"
    :initarg :quit-message
    :accessor quit-message
    :type string
    :documentation "A short message to say goodbye")
   (data-dir
    :initform *default-data-directory*
    :initarg :data-dir
    :accessor data-dir
    :documentation "the directory where the bot and plugins will store there files")
   (command-prefix
    :initform "!"
    :initarg :command-prefix
    :accessor command-prefix
    :documentation "Control what the bot recognizes as commands")
   (plugin-blacklist
    :initform nil
    :initarg :plugin-blacklist
    :accessor plugin-blacklist
    :documentation "Alist of (channel . plugins) pairs. Plugins can also be :all")
   (plugin-whitelist
    :initform nil
    :initarg :plugin-whitelist
    :accessor plugin-whitelist
    :documentation "Alist of (channel . plugins) pairs.
This is applied after the blacklist")
   (action-scheduler
    :initform nil
    :documentation "Used for `run-in-event-loop'")
   (event-thread
    :initform nil
    :accessor event-thread
    :documentation "The thread where the event loop runs"))
  (:documentation "a irc bot"))

(defgeneric start (bot server &key port connected-cb)
  (:documentation "connect to server and enter read loop

CONNECTED-CB will be called with the bot as the single argument when the
connection to the irc server is established."))

(defgeneric stop (bot)
  (:documentation "disconnect from server"))

(defgeneric send (lines to bot &key actionp)
  (:documentation "send a privmsg to `to' (which can be a chan or a user).
If `actionp' is true, use the ctcp action command"))

(defgeneric join (bot channel)
  (:documentation "Join a channel"))

(defgeneric leave (bot channel &key message)
  (:documentation "Leave a channel"))

(defgeneric get-topic (bot channel)
  (:documentation "Return the channel topic"))

(defgeneric set-topic (bot channel topic)
  (:documentation "Set the channel topic"))

(defgeneric add-plugin (plugin bot)
  (:documentation "add `plugin' to the bot if there isn't already a plugin with the same name.
Returns the bot as first value. The second value is either `t' if the plugin was really
inserted, or `nil' otherwise."))

(defgeneric remove-plugin (plugin-name bot)
  (:documentation "remove the plugin with the name `plugin-name' from the bot.
Returns the bot as first value. The second value is either `t' if the plugin was really
removed, or `nil' otherwise."))

(defgeneric find-plugin (plugin-name bot)
  (:documentation "return the plugin with `plugin-name' if it exists and nil otherwise"))


(defgeneric add-plugins (bot &rest plugins)
  (:documentation "add `plugins' to the bot. Plugins can be instances of classes derived
from PLUGIN, names of classes derived from PLUGIN or lists of those including lists of
lists of ..."))

;; The next two functions rely on the context of *last-message*. They should only
;; be called from an implementation of handle-event or a command.

(defgeneric reply (texts &optional to-user-p)
  (:documentation "can be used by plugins to let the bot say something. `texts' can be a list of strings or a string.
If `to-user-p' is t, address the user of the last received message directly"))

(defgeneric action (texts)
  (:documentation "can be used by plugins to write a /me message"))

(defgeneric run-in-event-loop (bot action)
  (:documentation "Run `action' in the context of the event loop.

`action' must be a function accepting no arguments. It will be synchronously
called in the event loop of the bot where it can use all the gems provided by
cl-async. Also, it can interact with the IRC server without fear of
concurrency."))

(defgeneric exit-event-loop (bot)
  (:documentation "Forcefully exit from the event loop.

This will also terminate the event thread. \"Forcefully\" in this context means
that the bot doesn't do cleanup such as sending /quit. All sockets will be
closed, however."))

(defmacro with-event-loop (bot &body body)
  "Run body inside the context of the event loop.

See `run-in-event-loop' for details."
  `(run-in-event-loop ,bot (lambda () ,@body)))

 ;;;;;;;;;;;;;;;;;
;;               ;;
;; Plugin Class  ;;
;;               ;;
 ;;;;;;;;;;;;;;;;;

(defclass plugin ()
  ((name
    :initform "noname"
    :accessor name
    :initarg :name)
   (bot
    :initform nil
    :reader bot
    :initarg :bot))
  (:documentation "all plugins must derive from this class"))

(defgeneric handle-event (plugin event)
  (:documentation "plugins can implement this for the various events"))

(defgeneric help (plugin)
  (:documentation "called when the user requests help for a plugin"))

(defparameter *last-message* nil
  "this is bound to the last message to the bot during the execution of commands
or the `handle-event' methods of plugins")

(defparameter *command-argstring* nil
  "bound to the argument of a command before splitting")

(defun help-for-commands (plugin)
  "Print the lambda-lists and docstrings of all commands from `plugin'
To be used in the `help' method."
  (reply (mappend (curry #'help-for-command (bot plugin))
                  (commands plugin)) nil))

(defclass command ()
  ((name
    :initarg :name
    :accessor command-name
    :documentation "The thing, a query will be matched against. Can be a string or
a symbol")
   (function
    :initarg :function
    :accessor command-function
    :documentation "The actual function to call when the command is run")
   (lambda-list
    :initarg :lambda-list
    :accessor command-lambda-list
    :documentation "The lambda list of the function")
   (doc-string
    :initform nil
    :initarg :doc
    :accessor command-doc-string
    :documentation "The documentation for this command. Can for example be used in
a help plugin")))

(defun make-command (name lambda-list function &key doc)
  "create a new command `name' can be a string or a symbol"
  (make-instance 'command
                 :name (string-downcase (string name))
                 :function function
                 :lambda-list lambda-list
                 :doc doc))

(defgeneric commands (plugin)
  (:documentation "return a list of all commands of the plugin"))

(defgeneric add-command (plugin command)
  (:documentation "add or replace a command to the plugin"))

(defgeneric remove-command (plugin command)
  (:documentation "remove a command from the plugin"))

(defgeneric run-command (command &rest args)
  (:documentation "run this command"))

(defgeneric find-command (plugin command)
  (:documentation "return a command named by `command' from `plugin', or nil
if there is no such command"))

(defgeneric command-matches-p (command-designator command)
  (:documentation "return true if the command-name of `command'
matches the `command-designator' which can be a string, a symbol
of an instance of the command class."))

(defmacro defcommand (name ((plvar plclass) &rest args) &body body)
  "add a new command to the plugin `plclass'. The name can be a string or a symbol.
If the first element of body is a string, it will be used as docstring for the
new command."
  (let* ((doc (if (stringp (first body)) (first body) nil))
         (body (if (stringp (first body)) (rest body) body))
         (fun `(lambda (,plvar ,@args) ,@body)))
    `(add-command ',plclass
                  (make-command ',name ',args ,fun :doc ,doc))))

 ;;;;;;;;;;;;;;;
;;             ;;
;; User Class  ;;
;;             ;;
 ;;;;;;;;;;;;;;;

;; 'user interface' is to be implemented

(defclass user ()
  ((nick
    :initarg :nick
    :accessor nick)                     ; TODO: setf nick should be /nick-aware
   (username
    :initarg :username
    :reader name)
   (host
    :initarg :host
    :reader host)))

(defgeneric user-equal (user1 user2)
  (:documentation "compare users"))

(defgeneric hostmask (user)
  (:documentation "return a host mask of the form nick!username@host"))

(defun userp (object)
  (eq (type-of object) 'user))

(defun selfp (user bot)
  (string= (nick bot) (nick user)))

 ;;;;;;;;;;;;;;;;;
;;               ;;
;; Channel Class ;;
;;               ;;
 ;;;;;;;;;;;;;;;;;

(defclass channel ()
  ((name
    :initarg :name
    :accessor name)
   (topic
    :initform nil
    :initarg :topic
    :accessor topic)))

(defgeneric channel-equal (chan1 chan2)
  (:documentation "compare channels"))

(defun make-channel (name)
  (make-instance 'channel :name name))

(defun channelp (object)
  (eq (type-of object) 'channel))

(defun find-channel (bot channel-name)
  (find channel-name (channels bot) :key #'name :test #'string=))

 ;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                         ;;
;; Internal implementation ;;
;;                         ;;
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric handle-irc-message (type bot message))

(defmethod handle-irc-message ((type (eql :rpl-welcome)) bot msg)
  (declare (ignore msg))
  (dolist (chan (autojoin-channels bot))
    (irc:join (connection bot) chan)))

(defmethod handle-irc-message ((type (eql :privmsg)) bot msg)
  (let ((message (make-event type msg bot)))
    (handle-priv-message message)))

(defmethod handle-irc-message ((type (eql :join)) bot msg)
  (let ((*last-message* (make-event type msg bot)))
    (if (selfp (user *last-message*) bot)
        (pushnew (channel *last-message*) (slot-value bot 'channels) :test #'channel-equal)
        (call-event-handlers *last-message*))))

(defmethod handle-irc-message ((type (eql :part)) bot msg)
  (let ((*last-message* (make-event type msg bot)))
    (if (selfp (user *last-message*) bot)
        (removef (slot-value bot 'channels) (channel *last-message*) :test #'channel-equal)
        (call-event-handlers *last-message*))))

(defun update-channel-topic (message)
  (when-let (chan (channel message))
    (setf (topic chan) (new-topic message))))

(defmethod handle-irc-message ((type (eql :topic)) bot msg)
  (let ((*last-message* (make-event type msg bot)))
    (update-channel-topic *last-message*)
    (call-event-handlers *last-message*)))

(defmethod handle-irc-message ((type (eql :rpl-topic)) bot msg)
  (let ((*last-message* (make-event type msg bot)))
    (update-channel-topic *last-message*)
    (call-event-handlers *last-message*)))

(defmethod handle-irc-message ((type (eql :rpl-notopic)) bot msg)
  (let ((*last-message* (make-event type msg bot)))
    (update-channel-topic *last-message*)
    (call-event-handlers *last-message*)))

(defun handle-errors-in-command (bot command err)
  (reply (cons (format nil "error in ~a: ~a | Usage:"
                       (command-name command) err)
               (help-for-command bot command))))

(defun string-splitter ()
  (let ((separators '(#\' #\"))
        (last-sep nil)
	(escaped nil))
    (lambda (x)
      (cond
	(escaped (setf escaped nil))
        ((and last-sep
              (char= x last-sep)) (progn (setf last-sep nil) t))
        ((member x separators) (progn
                                 (if (not last-sep)
                                     (progn (setf last-sep x) t))))
	((char= x #\\) (setf escaped t) nil)
	((and (char= x #\Space)
	      (not last-sep))
	 t)
	(t nil)))))

(defun split-string (string)
  (let ((list (split-sequence:split-sequence-if (string-splitter)
					   string
					   :remove-empty-subseqs nil)))
    list))

(defun plugin-allowed-p (blacklist whitelist plugin)
  (labels ((is-plugin (arg)
             (typecase arg
               (string (string= (name plugin) arg))
               (t (eq arg (type-of plugin))))))
    (let ((blacklisted (or (member :all blacklist)
                           (find-if #'is-plugin blacklist)))
          (whitelisted (find-if #'is-plugin whitelist)))
      (or (not blacklisted) whitelisted))))

(defun merge-policy-list (channel list)
  (let ((all  (cdr (assoc :all list)))
        (chan (cdr (assoc channel list :test #'channel-equal))))
    (concatenate 'list (ensure-list all) (ensure-list chan))))

(defun bot-channel-policy (bot channel plugin)
  (let ((blacklist (merge-policy-list channel (plugin-blacklist bot)))
        (whitelist (merge-policy-list channel (plugin-whitelist bot))))
    (plugin-allowed-p blacklist whitelist plugin)))


(defun run-command-by-name (bot command &rest args)
  (when-let (plugin (find-if (rcurry #'find-command command)
                             (plugins bot)))
    (unless (and (typep *last-message* 'channel-message)
                 (not
                  (bot-channel-policy bot (channel *last-message*) plugin)))
      (bb:catcher
          (apply #'run-command (find-command plugin command) (cons plugin args))
        (condition (err)
          (handle-errors-in-command bot (find-command plugin command) err))))))

(defun call-commands (message command)
  (let* ((*last-message* message)
         (args-white (split-string command))
         (args (remove-if #'alexandria:emptyp args-white))
         (pos (position-if-not #'alexandria:emptyp (rest args-white)))
         (*command-argstring* (format nil "~{~a~^ ~}" (if pos (subseq args-white (1+ pos)) args-white))))
    (apply #'run-command-by-name (bot message) (first args) (rest args))))

(defun call-event-handlers (event)
  (dolist (p (plugins (bot event)))
    (unless (and (typep event 'channel-message)
                 (not
                  (bot-channel-policy (bot event) (channel event) p)))
      (handle-event p event))))

(defun is-message-a-command-p (nick prefix text)
  (ppcre:scan-to-strings
   (concatenate 'string "^(" nick "\\W+"
                "|" prefix ")(.*)")
   text))

(defun handle-priv-message (message)
  (with-slots (bot text sender) message
    (when (not (selfp sender bot)) ;; never respond to myself!!
      (let ((*last-message* message))
        (call-event-handlers message))
      (if (typep message 'channel-message)
	  (multiple-value-bind (match msg)
	      (is-message-a-command-p (nick bot)
                                      (command-prefix bot)
                                      text)
	    (when match (call-commands message (elt msg 1))))
	  (call-commands message text)))))

 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                              ;;
;; Implementations of generics  ;;
;;                              ;;
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defparameter *default-irc-port* 6667)

(defmethod start ((bot bot) server &key (port *default-irc-port*) connected-cb)
  (let ((stdout *standard-output*))
    (setf
     (event-thread bot)
     (bt:make-thread
      (lambda ()
        (let ((*standard-output* stdout))
          (as:with-event-loop ()
            (let ((conn (irc:connect server port
                                     :nick (nick bot)
                                     :user (nick bot)
                                     :logging-stream *debug*
                                     :ping-timeout (* 5 60)
                                     :event-cb (lambda (con event)
                                                 (declare (ignore con))
                                                 (format t "Event: ~a~%" event)
                                                 (as:exit-event-loop)))))

              (setf (slot-value bot 'connection) conn)
              (setf (slot-value bot 'channels) nil)
              (setf (slot-value bot 'action-scheduler) (make-instance 'scheduler))

              (labels ((handler (con msg)
                         (declare (ignore con))
                         (handle-irc-message (irc:message-command msg) bot msg)
                         t))
                (irc:add-handler conn :join #'handler)
                (irc:add-handler conn :privmsg #'handler)
                (irc:add-handler conn :part #'handler)
                (irc:add-handler conn :topic #'handler)
                (irc:add-handler conn :rpl-topic #'handler)
                (irc:add-handler conn :rpl-notopic #'handler)

                ;; This will only run once
                (irc:add-handler conn :rpl-welcome
                                 (lambda (con msg)
                                   (declare (ignore con))
                                   (when connected-cb
                                     (funcall connected-cb bot))
                                   (handle-irc-message :rpl-welcome bot msg)
                                   nil)))))))
      :name "Event-loop"))))

(defmethod stop ((bot bot))
  (with-event-loop bot
    (irc:quit (connection bot) (quit-message bot))
    (as:with-delay (2)
      (as:exit-event-loop)))
  (bt:join-thread (event-thread bot))
  (setf (event-thread bot) nil))

(defmethod send (lines to (bot bot) &key actionp)
  (let ((connection (connection bot))
        (to (cond
              ((userp to) (nick to))
              ((channelp to) (name to))
              (t to)))
        (lines (if actionp
                   (actionize-lines lines)
                 (ensure-list lines))))
    (when connection
      (loop for msg in lines
            for i from 1
            do (progn
                 (irc:privmsg connection to msg)
                 (when (= (mod i 3) 0)
                   (sleep 1)))))))

(defmethod join ((self bot) channel)
  (unless (find channel (channels self) :test #'string= :key #'name)
    (irc:join (connection self) channel)
    t))

(defmethod leave ((self bot) channel &key message)
  (when (find channel (channels self) :test #'string= :key #'name)
    (irc:part (connection self) channel (or message (quit-message self)))
    t))

(defmethod get-topic ((self bot) (channel channel))
  (declare (ignore self))
  (topic channel))

(defmethod get-topic ((self bot) channel)
  (when-let (channel (find-channel self channel))
    (topic channel)))

(defmethod set-topic ((self bot) channel topic)
  (when-let (find-channel self channel)
    (irc:topic (connection self) channel topic)))

(defmethod handle-event ((plugin plugin) (event event))
  (declare (ignore plugin event))
  nil)

(defgeneric reply-to-event (message lines &optional to-user-p))

(defun address-user (lines nick)
  (mapcar (lambda (x)
            (format nil "~a: ~a" nick x))
          (ensure-list lines)))

(defmethod reply-to-event ((message channel-message) lines &optional to-user-p)
  (send (if to-user-p
            (address-user lines (nick (sender message)))
          lines)
        (channel message) (bot message)))

(defmethod reply-to-event ((message query-message) lines &optional to-user-p)
  (declare (ignore to-user-p))
  (send lines (sender message) (bot message)))

(defmethod reply-to-event ((event join-event) lines &optional to-user-p)
  (send (if to-user-p
            (address-user lines (nick (user event)))
          lines)
        (channel event) (bot event)))

(defmethod reply-to-event ((event (eql nil)) lines &optional to-user-p)
  (declare (ignore event lines to-user-p)))

(defmethod reply (lines &optional to-user-p)
  (reply-to-event *last-message* lines to-user-p))

(defparameter *ctcp-delimiter* (code-char 1))

(defun actionize-lines (lines)
  (mapcar (lambda (x)
            (format nil "~aACTION ~a~@*~a~*" *ctcp-delimiter* x))
          (ensure-list lines)))

(defmethod action (lines)
  (reply-to-event *last-message* (actionize-lines lines)))

(defmethod help ((plugin plugin))
  (declare (ignore plugin))
  :unimplemented-help)

(defmethod user-equal ((user1 user) (user2 user))
  (and (string-equal (nick user1) (nick user2))
       (string-equal (host user1) (host user2))
       (string-equal (name user1) (name user2))))

;; TODO: also specialize the hostmask function on the bot class
(defmethod hostmask ((user user))
  (format nil
          "~a!~a@~a"
          (nick user)
          (name user)
          (host user)))

(defmethod channel-equal ((chan1 channel) (chan2 channel))
  (string-equal (name chan1) (name chan2)))

(defmethod channel-equal ((chan1 string) (chan2 channel))
  (string-equal chan1 (name chan2)))

(defmethod channel-equal ((chan1 channel) (chan2 string))
  (string-equal (name chan1) chan2))

(defmethod channel-equal ((chan1 string) (chan2 string))
  (string-equal chan1 chan2))

;;; This is needed for comparing :all to a channel. meh
(defmethod channel-equal (chan1 chan2)
  nil)

(defmethod add-plugin ((plugin plugin) (bot bot))
  (values bot (unless (find-plugin (name plugin) bot)
                (push plugin (plugins bot))
                t)))

(defmethod remove-plugin (name (bot bot))
  (values bot
          (when (find-plugin name bot)
            (setf (plugins bot)
                  (delete-if (plugin-name= name) (plugins bot)))
            t)))

(defmethod find-plugin (name (bot bot))
  (find-if (plugin-name= name) (plugins bot)))

(defun plugin-name= (name)
  (lambda (plugin) (string= (name plugin) name)))

(defmethod add-plugins ((self bot) &rest plugins)
  (labels ((make-plugins (plugins)
                         (loop for p in plugins appending
                               (cond
                                ((listp p) (make-plugins p))
                                ((symbolp p) (ensure-list (make-instance p :bot self)))
                                ((subtypep (type-of p) 'plugin) (progn
                                                                  (setf (slot-value p 'bot) self)
                                                                  (ensure-list p)))
                                (t (error "strange plugin: ~a" p))))))
    (reduce #'add-plugin (make-plugins plugins) :initial-value self
            :from-end t)))

(defmethod initialize-instance :after ((bot bot) &key plugins autojoin)
  (apply #'add-plugins bot plugins)
  (setf (slot-value bot 'autojoin-channels) (ensure-list autojoin)))

;; Plugins and commands
(defun ensure-plugin-symbol (thing)
  (if (symbolp thing) thing (type-of thing)))

(defmethod commands (self)
  (get (ensure-plugin-symbol self) :commands))

(defmethod add-command (plugin (cmd command))
  (setf (get (ensure-plugin-symbol plugin) :commands)
        (cons cmd (delete cmd (get (ensure-plugin-symbol plugin) :commands)
                          :test #'command-matches-p))))

(defmethod remove-command (plugin cmd)
  (setf (get (ensure-plugin-symbol plugin) :commands)
        (delete cmd (get (ensure-plugin-symbol plugin) :commands)
                :test #'command-matches-p)))

(defmethod run-command ((self command) &rest args)
  (apply (command-function self) args))

(defmethod find-command (plugin command)
  (find command (commands plugin) :test #'command-matches-p))

(defmethod command-matches-p ((s string) (self command))
  (string= (command-name self) s))

(defmethod command-matches-p ((s symbol) (self command))
  (string-equal (command-name self) (string-downcase (symbol-name s))))

(defmethod command-matches-p ((other command) (self command))
  (string-equal (command-name self)
                (command-name other)))

(defun help-for-command (bot command)
  (let ((doclines (split-sequence:split-sequence
                   #\Newline (command-doc-string command)
                   :remove-empty-subseqs t)))
    (cons (format nil "~a~a~{~^ ~(~a~)~}: ~a" (command-prefix bot)
                  (command-name command)
                  (command-lambda-list command)
                  (or (first doclines) "no help available"))
          (rest doclines))))

(defmethod run-in-event-loop ((self bot) action)
  (schedule-action (slot-value self 'action-scheduler) action))

(defmethod exit-event-loop ((self bot))
  (run-in-event-loop self #'as:exit-event-loop))

(defmethod print-object ((object bot) s)
  (print-unreadable-object (object s :type t)
                           (princ (nick object) s)))

(defmethod print-object ((object plugin) s)
  (print-unreadable-object (object s :type t)
                           (princ (name object) s)))

(defmethod print-object ((object channel) s)
  (print-unreadable-object (object s :type t)
    (format s "~a" (name object))))
