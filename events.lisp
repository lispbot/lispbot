;; Events are organized as a class hirarchy. Plugins can specialize
;; the generic function HANDLE-EVENT on one of these classes to
;; receive the events it is interested in.

(in-package :bot)

(defclass event ()
  ((time
    :initarg :time
    :initform (get-universal-time)
    :reader timestamp
    :documentation "The time when this event was received")
   (bot
    :initarg :bot
    :reader bot
    :documentation "The bot, that received this event"))
  (:documentation "base class for all events"))

(defgeneric make-event (msg-type irc-message bot)
  (:documentation "create the right event object, depending on the specified cl-irc-message class"))

(defclass message (event)
  ((text
    :initarg :text
    :accessor text
    :documentation "The message text")
   (sender
    :initarg :from
    :initarg :sender
    :reader sender
    :documentation "The guy who sent this message"))
  (:documentation "A message from another user that contain text"))

(defclass channel-message (message)
  ((channel
    :initarg :channel
    :reader channel
    :documentation "The channel the message was received in"))
  (:documentation "Normal message exchanged in a channel"))

(defclass query-message (message)
  ()
  (:documentation "Message received as private message (read query)"))

(defun extract-user-from-irc-message (message)
  (make-instance 'user
		 :nick (irc:message-user-nick message)
		 :host (irc:message-user-host message)
		 :username (irc:message-user-user message)))

(defmethod make-event ((type (eql :privmsg)) message bot)
  (let ((channel (find (elt (irc:message-params message) 0) (channels bot) :test #'string= :key #'name))
	(text (car (last (irc:message-params message))))
	(sender (extract-user-from-irc-message message)))
    (if channel
	(make-instance 'channel-message
		       :text text
		       :from sender
		       :bot bot
		       :channel channel)
	(make-instance 'query-message
		       :text text
		       :from sender
		       :bot bot))))

(defclass user-event (event)
  ((user
    :initarg :user
    :reader user
    :documentation "The user who caused this event"))
  (:documentation "Base class for all events that have something to do with user-management"))

(defclass join-event (user-event)
  ((channel
    :initarg :channel
    :reader channel
    :documentation "The channel that was joined")))

(defmethod make-event ((type (eql :join)) message bot)
  (make-instance 'join-event
		 :user (extract-user-from-irc-message message)
		 :channel (make-channel (car (irc:message-params message)))
		 :bot bot))

(defclass part-event (user-event)
  ((channel
    :initarg :channel
    :reader channel
    :documentation "The channel that was left")
   (message
    :initarg :message
    :reader part-message
    :documentation "part message")))

(defmethod make-event ((type (eql :part)) message bot)
  (make-instance 'part-event
		 :user (extract-user-from-irc-message message)
                 :channel (car (irc:message-params message))
		 :message (car (last (irc:message-params message)))
		 :bot bot))

(defclass topic-event (user-event)
  ((channel
    :initarg :channel
    :reader channel
    :documentation "The channel where the topic was changed")
   (new-topic
    :initarg :topic
    :reader new-topic
    :documentation "The topic that was set. Can be empty")))

(defmethod make-event ((type (eql :topic)) message bot)
  (make-instance 'topic-event
                 :user (extract-user-from-irc-message message)
                 :channel (find-channel bot (first (irc:message-params message)))
                 :topic (second (irc:message-params message))
                 :bot bot))

(defmethod make-event ((type (eql :rpl-topic)) message bot)
  (make-instance 'topic-event
                 :user (extract-user-from-irc-message message)
                 :channel (find-channel bot (second (irc:message-params message)))
                 :topic (third (irc:message-params message))
                 :bot bot))

(defmethod make-event ((type (eql :rpl-notopic)) message bot)
  (make-instance 'topic-event
                 :user (extract-user-from-irc-message message)
                 :channel (find-channel bot (second (irc:message-params message)))
                 :topic ""
                 :bot bot))
