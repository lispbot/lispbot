(in-package :lispbot)

(defclass test-bot (bot)
  ((luser :initform (make-instance 'user :nick "luser") :accessor test-bot-luser)
   (topic :initform "There is no topic" :accessor test-bot-topic)
   (buffer :initform (make-array 0
                                 :element-type 'base-char
                                 :adjustable t
                                 :fill-pointer 0) :accessor test-bot-buffer)))

(defparameter *testbot-commands*
  '(("/nick" . test-bot-/nick)
    ("/topic" . test-bot-/topic)
    ("/exit" . test-bot-/exit)))

(defmethod start ((bot test-bot) _ &key port connected-cb)
  (declare (ignore _ port))
  (as:with-event-loop ()
    (format t "Lispbot testing repl: Use /exit to leave~%")
    (as:interval (lambda () (test-bot-check-read bot)) :time 1)
    (funcall connected-cb bot)
    (princ "testbot-repl> ")
    (as:with-delay (20)
      (as:exit-event-loop))))

(defun test-bot-handle-msg (bot l chan)
  (let ((args (split-string l)))
    (if-let (cmd (assoc (first args) *testbot-commands* :test #'string-equal))
      (apply (cdr cmd) bot (rest args))
      (handle-priv-message (make-instance 'channel-message
                                          :text l
                                          :from (test-bot-luser bot)
                                          :bot bot
                                          :channel chan)))))

(defmethod stop ((bot test-bot))
  (declare (ignore bot))
  (error "stop not defined for test-bot"))

(defmethod send (lines to (bot test-bot) &key actionp)
  (declare (ignore to))
  (dolist (l (if actionp (actionize-lines lines) (ensure-list lines)))
    (princ l) (terpri)))

(defmethod leave ((self test-bot) _ &key message)
  (declare (ignore self _ message))
  (values))

(defmethod join ((self test-bot) channel)
  (declare (ignore self channel))
  (values))

(defmethod get-topic ((self test-bot) channel)
  (test-bot-topic self))

(defmethod set-topic ((self test-bot) channel topic)
  (setf (test-bot-topic self) topic))

(defun start-test-bot (plugins)
  (start (make-instance 'test-bot :plugins plugins) nil))

;;; internal commands

(defun test-bot-/nick (bot nick)
  (format t "changed nick to ~a~%" nick)
  (setf (nick (test-bot-luser bot)) nick))

(defun test-bot-/topic (bot &optional topic)
  (if topic
      (progn
        (set-topic bot "" topic)
        (format t "Topic changed to ~a~%" topic))
      (format t "~a~%" (get-topic bot ""))))

(defun test-bot-/exit (bot)
  (declare (ignore bot))
  (as:exit-event-loop))

(defun test-bot-check-read (bot)
  (loop while
       (when-let (char (read-char-no-hang *standard-input* nil))
         (if (char= char #\Newline)
             (progn
               (test-bot-handle-msg bot (test-bot-buffer bot) "repl")
               (setf (fill-pointer (test-bot-buffer bot)) 0)
               (princ "testbot-repl> "))
             (vector-push-extend char (test-bot-buffer bot))))))
